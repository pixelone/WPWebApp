<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 09/12/2016
 * Time: 23:04
 */

namespace WPWebApp\controller;

use WPWebApp\tools\Notifications;
use WPWebApp\tools\UserSubscription;

class PublicEndpoint{

    public function __construct() {}

    public function rewriteUrl(){
        add_rewrite_tag('%registeringEndpoint%', '([^/]+)');
        add_rewrite_rule( 'registeringEndpoint', 'index.php?registeringEndpoint=true', 'top' );
        add_rewrite_tag('%notificationInformation%', '([^/]+)');
        add_rewrite_rule( 'notificationInformation', 'index.php?notificationInformation=true', 'top' );
    }

    public function addQueryVars($query_vars){
        $query_vars[] = 'registeringEndpoint';
        return $query_vars;
    }

    public function processQueryVars(&$wp){
        if ( array_key_exists( 'registeringEndpoint', $wp->query_vars ) ) {
            wp_send_json($this->tokenRegisteringEndpoint());
        } elseif(array_key_exists( 'notificationInformation', $wp->query_vars )){
            wp_send_json($this->notificationInformation());
        }
        return;
    }

    public function tokenRegisteringEndpoint(){
        //CHECK IF CGMID exists
        //1 - User generate a token and send it to us
        if(!isset($_POST['subscriptionData'])) return null;
        $subscriptionData = json_decode(stripslashes($_POST['subscriptionData']));
        //$registration = new UserSubscription();
        $subscriptionData->GCMID = explode('/',$subscriptionData->endpoint)[5];
        //$subscriptionId = $_POST['subscriptionUserToken'];
        //2 - We generate a key that we send to the subscriptionGCMID
        //3 - User get a hidden notification
        //4 - User process TokenAndKey & send us the result
        //5 - We check if the result match ours
        //6 - Save or reject subscriptionGCMID
        //$toRegister = $registration->handleGcmId($subscriptionData);
        $toRegister = true;
        if($toRegister){
            $notification = new Notifications($subscriptionData->GCMID, $subscriptionData->keys->p256dh, $subscriptionData->keys->auth);
            $result = $notification->sendFCMNotifications();
        } else {
            $result = array('Alreay Registered');
        }
        return $result;
    }

    public function notificationInformation(){
        $param = array(
            'numberposts' => 1,
            'offset' => 0,
            'category' => 0,
            'orderby' => 'post_date',
            'order' => 'DESC',
            'post_type' => 'post',
            'post_status' => 'publish',
        );
        $lastPost = wp_get_recent_posts( $param, OBJECT );
        $lastPost = $lastPost[key($lastPost)];
        $lastPost = array(
            'title' => $lastPost->post_title,
            'post_content' => $string = str_replace("\r\n",' ',trim(wp_strip_all_tags($lastPost->post_content))),
            'link' => get_permalink($lastPost)
        );
        return $lastPost;
    }
}