<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 09/12/2016
 * Time: 22:22
 */

namespace WPWebApp\controller;

class Settings
{

    CONST PAGENAME = 'settingsPage';
    public $parameterList = array(
        'wpwebapp_gcm_sender_id' => 'GCM Sender ID',
        'wpwebapp_gcm_api_key' => 'GCM API Key',
    );

    public function __construct() {

    }

    public function addToMenu(){
        \add_menu_page(
            'WP Web App Settings',
            'WebApp Settings',
            'administrator',
            Settings::PAGENAME,
            array($this, 'generateMainForm')
        );
    }

    public function initAdminPage(){
        $settingsSection = Settings::PAGENAME.'_section';
        $availableSettings = $this->parameterList;

        \add_settings_section(
            $settingsSection,
            'Main Settings',
            null,
            Settings::PAGENAME
        );


        function generateFormInput($param){
            $value = get_option($param[0]);
            print "Input <input type='text' name='$param[0]' value='$value'>";
        }

        foreach ($availableSettings as $settingKey => $settingName){
            \register_setting($settingsSection, $settingKey);
            \add_settings_field(
                $settingKey,
                $settingName,
                array($this, 'generateFormInput'),
                Settings::PAGENAME,
                $settingsSection,
                array($settingKey)
            );
        }
    }

    public function generateFormInput($param){
        $value = get_option($param[0]);
        echo "<input type='text' name='$param[0]' value='$value'>";
    }

    public function generateMainForm(){
        ?> <div class="wrap">
             <h1> <?php esc_html(get_admin_page_title()); ?></h1>
            <form action="options.php" method="post">
                <?php settings_fields(Settings::PAGENAME.'_section');
                echo 'Settings Sections:';
                do_settings_sections(Settings::PAGENAME);
                submit_button();
                ?>
            </form>
            </div>
        <?php
    }
}