<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 09/12/2016
 * Time: 22:22
 */

namespace WPWebApp\controller;
use WPWebApp\tools\Notifications;
use WPWebApp\tools\UserSubscription;

class NotificationsTest
{

    const PAGENAME = 'notificationsTest';
    private $wpdb;

    public function __construct() {
        global $wpdb;
        $this->wpdb = $wpdb;
    }

    public function addToMenu(){
        \add_submenu_page(
            Settings::PAGENAME,
            'WP Web App Notification Test',
            'WebApp Notification Test',
            'administrator',
            NotificationsTest::PAGENAME,
            array($this, 'generateTestDashboard')
        );
    }

    public function initAdminPage(){

    }

    public function generateTestDashboard(){
        $tableName = $this->wpdb->prefix.UserSubscription::TABLE_SUFFIX;
        if(isset($_GET['testNotificationID'])){
            $userSub = UserSubscription::fetchByID($_GET['testNotificationID']);
            if(is_object($userSub)){
                $notif = new Notifications(array($userSub->gcmid));
                $notif->sendFCMNotifications();
            }
        }
        ?>
        <div class="wrap">
            <style>
                dl dt, dl dd{
                    display: inline-block;
                    padding: 0 20px;
                }
            </style>
            <h1>Test Notifications</h1>
            <?php
            $userSubscriptions = $this->wpdb->get_results("SELECT * FROM $tableName");
            foreach ( $userSubscriptions as $userSubscription ) {
                echo "<dl title='$userSubscription->gcmid'><dt>$userSubscription->id</dt>";
                echo "<dt>$userSubscription->status</dt>";
                echo "<dt>$userSubscription->created</dt>";
                echo "<dt>$userSubscription->updated</dt>";
                echo "<dt><a href='/wp-admin/admin.php?page=notificationsTest&testNotificationID=$userSubscription->id'>Test Notitfication</a></dt>";
                echo "</dl>";
            }
            ?>
        </div>
        <?php
    }
}