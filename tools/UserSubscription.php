<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 18/12/2016
 * Time: 13:29
 */

namespace WPWebApp\tools;

class UserSubscription
{
    const TABLE_SUFFIX = 'wpwebapp_subscription';

    public $tableName;
    public $wpdb;

    public function __construct() {
        global $wpdb;
        $this->wpdb = &$wpdb;
        $this->tableName = $this->wpdb->prefix . UserSubscription::TABLE_SUFFIX;
    }

    /**
     * @param $gcmId
     * @return bool True if the GCM is New
     */
    public function handleGcmId($gcmId){
        $gcmId = stripslashes(htmlentities($gcmId));

        $existingrecord = $this->wpdb->get_row($this->wpdb->prepare("SELECT * FROM $this->tableName WHERE gcmid = %s;", $gcmId));
        if(is_null($existingrecord)){
            $this->wpdb->insert(
                $this->tableName,
                array(
                    'gcmid' => $gcmId,
                    'status' => 'created',
                    'created' => date("Y-m-d H:i:s"),
                    'updated' => date("Y-m-d H:i:s")
                )
            );
            return true;
        } else {
            $this->wpdb->update(
                $this->tableName,
                array(
                    'updated' => date("Y-m-d H:i:s")
                ),
                array(
                    'gcmid' => $gcmId
                )
            );
            return false;
        }

    }

    static function fetchByID($id){
        $userSub = new self;
        return $userSub->wpdb->get_row($userSub->wpdb->prepare("SELECT * FROM $userSub->tableName WHERE id = %d;", $id));
    }

    public function fetchActiveSubscriptions(){
        return $this->wpdb->get_col("SELECT gcmid FROM $this->tableName WHERE status != 'disabled'");
    }
}