<?php

/*
Plugin Name: WPWeb App
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: A brief description of the Plugin.
Version: 1.0
Author: rober
Author URI: http://URI_Of_The_Plugin_Author
License: A "Slug" license name e.g. GPL2
*/
namespace WPWebApp;
use WPWebApp\controller\Settings;

require_once("event/Listener.php");
require_once('event/Publish.php');
require_once("controller/Settings.php");
require_once("controller/NotificationsTest.php");
require_once("controller/PublicEndpoint.php");
require_once('tools/UserSubscription.php');
require_once('tools/Notifications.php');
require_once('tools/Base64Url.php');
require('vendor/autoload.php');

class WPWebApp {
    function __construct() {
        $this->listener = new event\Listener();
    }

}
$WPWebApp = new WPWebApp();

function activateWebAppPlugin() {
    flush_rewrite_rules();
    $Settings = new Settings();
    foreach ($Settings->parameterList as $paramKey => $paramName){
        add_option($paramKey);
    }

    //Create DB
    global $wpdb;
    $table_name = $wpdb->prefix . 'wpwebapp';
    $charset_collate = $wpdb->get_charset_collate();
    $sql = "CREATE TABLE {$table_name} (
		id mediumint(9) unsigned NOT NULL AUTO_INCREMENT,
		gcmid varchar(255) NULL,
		status TINYTEXT(128) NULL,
		created datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		updated datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		PRIMARY KEY  (id)
	) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
}

register_activation_hook( __FILE__, 'WPWebApp\activateWebAppPlugin' );