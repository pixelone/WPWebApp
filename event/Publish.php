<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 18/12/2016
 * Time: 14:03
 */

namespace WPWebApp\event;


use WPWebApp\tools\Notifications;
use WPWebApp\tools\UserSubscription;

class Publish{

    public $wpdb;

    public function __construct(){
        global $wpdb;
        $this->wpdb = &$wpdb;
    }

    public function onPublish($PostID){
        $hasNotification = get_post_meta($PostID, "webapp_notification", true);
        if(!$hasNotification){
            $users = new UserSubscription();
            $notification = new Notifications($users->fetchActiveSubscriptions());
            $notification->sendFCMNotifications();
            update_post_meta($PostID, 'webapp_notification', true);
        }
    }
}