<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 09/12/2016
 * Time: 22:21
 */
namespace WPWebApp\event;

class Listener {
    public function __construct() {
        $this->add_actions();
    }

    private function add_actions() {
        \add_action('admin_menu', array(new \WPWebApp\controller\Settings(), 'addToMenu'));
        \add_action('admin_menu', array(new \WPWebApp\controller\NotificationsTest(), 'addToMenu'));
        \add_action('admin_init', array(new \WPWebApp\controller\Settings(), 'initAdminPage'));
        \add_action('init', array(new \WPWebApp\controller\PublicEndpoint(), 'rewriteUrl'));
        \add_action('query_vars', array(new \WPWebApp\controller\PublicEndpoint(), 'addQueryVars'));
        \add_action('parse_request', array(new \WPWebApp\controller\PublicEndpoint(), 'processQueryVars') );
        \add_action('publish_post', array(new \WPWebApp\event\Publish(), 'onPublish' ));
    }

}